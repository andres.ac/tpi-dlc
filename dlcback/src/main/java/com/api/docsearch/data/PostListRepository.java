package com.api.docsearch.data;

import com.api.docsearch.models.Document;
import com.api.docsearch.models.PostList;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;

@Repository
public interface PostListRepository extends CrudRepository<PostList, Long> {

    @Query(
            value = "SELECT * FROM postlist p WHERE p.id_word IN :idWords",
            nativeQuery = true)
    Collection<PostList> findPostlistByWords(@Param("idWords") Integer [] idWords);
}
