package com.api.docsearch.data;

import com.api.docsearch.models.Document;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {
    @Query(
            value = "SELECT * FROM documents d WHERE d.id_document IN :docIds",
            nativeQuery = true)
    Collection<Document> findDocumentsById(@Param("docIds") Integer[] docIds);

    @Query(
            value = "SELECT count(d.id_document) FROM documents d WHERE d.id_document = :docName",
            nativeQuery = true)
    Integer existeArchivo(@Param("docName") String docName);

    @Query(
            value = "SELECT * FROM documents d",
            nativeQuery = true)
    Collection<Document> obtenerDocumentosCargados();

    @Query(
            value = "SELECT count(d.id_document) FROM documents d",
            nativeQuery = true)
    Integer cantidadDocumentos();
}
