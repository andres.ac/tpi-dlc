package com.api.docsearch.data;

import com.api.docsearch.models.PostList;
import com.api.docsearch.models.VocabularyEntry;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Stream;

@Repository
public interface VocabularyRepository extends CrudRepository<VocabularyEntry, Long> {
    @Query(
            value = "SELECT v.id_word FROM vocabulary v WHERE v.word = :name",
            nativeQuery = true)
    Integer findIdWord(@Param("name") String name);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(
            value = "UPDATE vocabulary t1, (SELECT id_word,COUNT(DISTINCT(id_document)) as cant_doc FROM postlist GROUP BY id_word) as t2 SET max_documents = t2.cant_doc WHERE t1.id_word = t2.id_word",
            nativeQuery = true)
    void setMaxDocuments();
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(
            value = "UPDATE vocabulary t1, (SELECT id_word, MAX(frequency) as frequency FROM postlist t3 GROUP BY id_word) as t2 SET max_frequency = t2.frequency WHERE t1.id_word = t2.id_word",
            nativeQuery = true)
    void setMaxFrequency();

    @Query(
            value = "SELECT * FROM vocabulary v WHERE v.word IN :wordsList",
            nativeQuery = true)
    Collection<VocabularyEntry> findWordsByValue(@Param("wordsList") String [] wordsList);

    @Query(
            value = "SELECT * FROM vocabulary v",
            nativeQuery = true)
    Collection<VocabularyEntry> obtenerVocabularioActual();

    @Query(
            value = "SELECT max(v.id_word) FROM vocabulary v",
            nativeQuery = true)
    Integer obtenerUltimoId();
}
