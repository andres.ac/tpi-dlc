package com.api.docsearch.service;

import com.api.docsearch.builders.Flusher;
import com.api.docsearch.builders.Indexer;
import com.api.docsearch.data.DocumentRepository;
import com.api.docsearch.googledrive.DriveDownloader;
import com.api.docsearch.googledrive.DriveService;
import com.api.docsearch.models.Document;
import com.google.api.services.drive.model.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Service("searchService")
public class SearchServiceImpl implements SearchService {
    @Autowired
    DriveService driveService;
    @Autowired
    DriveDownloader driveDownloader;
    @Autowired
    ArrayList<Document> documentos;
    @Autowired
    Boolean termino;
    @Autowired
    Indexer indexerService;
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    public ConcurrentHashMap<String, Integer> vocabulary;
    @Autowired
    Flusher flusherService;

    @Async
    @Override
    public void indexar() throws IOException, GeneralSecurityException, InterruptedException {
        List<File> lsFiles = driveService.getFilesDrive();
/*        if (driveDownloader.getDocumentos().size() > 0) {
            for (Document d : driveDownloader.getDocumentos()) {
                File toRemove = lsFiles.stream().filter(n -> n.getName().equals(d.getDocName())).findFirst().get();
                lsFiles.remove(toRemove);
            }

        }*/
        if (documentRepository.obtenerDocumentosCargados().size() > 0 ) {
          for (Document d : documentRepository.obtenerDocumentosCargados()) {
            File toRemove = lsFiles.stream().filter(n -> n.getName().equals(d.getDocName())).findFirst().get();
                lsFiles.remove(toRemove);
            }
        }
        for (int i = 0; i < lsFiles.size(); i++) {
            leer(lsFiles.get(i));
            if (lsFiles.size() == driveDownloader.getDocumentos().size()) {
               termino = true;
            }
        }
        flusherService.flush();
    }

    public void leer(File file) throws IOException, InterruptedException {
        driveDownloader.download(file);
    }

    public boolean isFinished() {
        return (termino && documentos.isEmpty());
    }

    public void setFinishedFalse() {
        termino = false;
    }

    public Document getNext() {
        return documentos.size() != 0 ? documentos.remove(documentos.size() - 1) : null;
    }

}