package com.api.docsearch.service;

import com.api.docsearch.models.Document;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public interface WordService {

    Document[] buscar(String palabra);
    Integer setMax();
    ConcurrentHashMap<String, Integer> obtenerVocabularioActual ();
    Integer obtenerUltimoId();
    AtomicInteger cantidadDocumentos();
    ArrayList<Document> obtenerDocumentosCargados();
}
