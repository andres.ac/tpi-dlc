package com.api.docsearch.service;

import com.api.docsearch.data.DocumentRepository;
import com.api.docsearch.data.PostListRepository;
import com.api.docsearch.data.VocabularyRepository;
import com.api.docsearch.models.Document;
import com.api.docsearch.models.PostList;
import com.api.docsearch.models.VocabularyEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Service("wordService")
public class WordServiceImpl implements WordService {

    @Autowired
    PostListRepository postListRepository;
    @Autowired
    VocabularyRepository vocabularyRepository;
    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    private AtomicInteger NUMBER_OF_DOCUMENTS;

    private ConcurrentHashMap<Integer, VocabularyEntry> words;
    private Hashtable<Integer, Document> documents;
    private ArrayList<PostList> postlists;

    private ArrayList<Document> sortedDocuments;

    public boolean search(String s) {
        sortedDocuments = new ArrayList<>();
        setWords(s);
        if (words.size() != 0) {
            setPlist();
            setDocuments();
            calculateRanking();
            sortDocuments();
            return true;
        }
        return false;
    }

    private void sortDocuments() {
        Collections.sort(sortedDocuments, (o1, o2) -> Float.compare(o2.getRanking(), o1.getRanking()));
    }

    public Document[] getSortedDocuments() {
        return sortedDocuments.toArray(new Document[sortedDocuments.size()]);
    }


    public void setWords(String q) {
        words = new ConcurrentHashMap<>();
        String[] w = splitString(q);
        ArrayList<VocabularyEntry> arrayWords = new ArrayList<>(vocabularyRepository.findWordsByValue(w));
        for (VocabularyEntry a : arrayWords) {
            if (!words.containsKey(a.getIdWord()))
                words.put(a.getIdWord(), a);
        }
    }

    private Integer[] getIdWords() {
        return words.keySet().toArray(new Integer[words.size()]);
    }

    private void setPlist() {
        postlists = new ArrayList<>(postListRepository.findPostlistByWords(getIdWords()));
    }

    private Integer[] getIdDocuments() {
        ArrayList<Integer> idDoc = new ArrayList<>();
        for (PostList pl : postlists) {
            if (!idDoc.contains(pl.getIdDocument())) idDoc.add(pl.getIdDocument());
        }
        return idDoc.toArray(new Integer[idDoc.size()]);
    }

    private void setDocuments() {
        documents = new Hashtable<>();
        ArrayList<Document> docs = new ArrayList<>(documentRepository.findDocumentsById(getIdDocuments()));
        for (Document d : docs) {
            documents.put(d.getIdDocument(), d);
        }
    }

    private void calculateRanking() {
        sortedDocuments = new ArrayList<>();
        Document docTemp;
        float rankTemp = 0;
        for (Integer idDoc : documents.keySet().toArray(new Integer[documents.size()])) {
            docTemp = documents.get(idDoc);
            rankTemp = calculateDocumentRanking(docTemp.getIdDocument(), rankTemp);
            docTemp.setRanking(rankTemp);
            sortedDocuments.add(docTemp);
        }
    }

    private float calculateIndividualRanking(Integer maxDocuments, Integer frequency) {
        return (float) frequency * (float) Math.log10((float) NUMBER_OF_DOCUMENTS.get() / (float) maxDocuments);
    }

    private float calculateDocumentRanking(int idDoc, float rankTemp) {
        rankTemp = 0;
        for (PostList pl : postlists) {
            if (pl.getIdDocument() == idDoc) {
                rankTemp += calculateIndividualRanking(words.get(pl.getIdWord()).getMaxDocuments(), pl.getFrequency());
            }
        }
        return rankTemp;
    }

    public String[] splitString(String s) {
        String[] str = s.split("[^\\p{Alpha}]");
        for (int i = 0; i < str.length; i++) {
            str[i] = checkWord(str[i]);
        }
        return str;
    }

    private String checkWord(String word) {
        word = word.replaceAll("([.,\\-\"()'°ª:;¿?_*|~€¬&=!¡<>\\[\\]#@«»$%]|[0-9])+", " ");
        return word.toLowerCase();
    }

    public Document[] buscar(String palabra) {
        search(palabra);
        return getSortedDocuments();
    }

    public Integer setMax() {
        vocabularyRepository.setMaxDocuments();
        vocabularyRepository.setMaxFrequency();
        cantidadDocumentos();
        return 1;
    }

    public ConcurrentHashMap<String, Integer> obtenerVocabularioActual() {
        ConcurrentHashMap<String, Integer> vocabulary = new ConcurrentHashMap<>();
        vocabularyRepository.obtenerVocabularioActual().stream().forEach((p) -> {
            vocabulary.put(p.getWord(), p.getIdWord());
        });
        return vocabulary;
    }

    public Integer obtenerUltimoId(){
        return  vocabularyRepository.obtenerUltimoId();
    }

    public ArrayList<Document> obtenerDocumentosCargados(){
       return new ArrayList(documentRepository.obtenerDocumentosCargados());
    }

    @PostConstruct
    public AtomicInteger cantidadDocumentos(){

        return NUMBER_OF_DOCUMENTS = new AtomicInteger(documentRepository.cantidadDocumentos());

    }
}