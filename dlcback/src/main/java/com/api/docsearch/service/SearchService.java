package com.api.docsearch.service;

import com.api.docsearch.models.Document;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public interface SearchService {

    void indexar() throws IOException, GeneralSecurityException, InterruptedException;
}
