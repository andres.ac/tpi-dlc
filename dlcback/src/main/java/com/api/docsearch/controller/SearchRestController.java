package com.api.docsearch.controller;

import com.api.docsearch.models.Document;
import com.api.docsearch.service.SearchService;
import com.api.docsearch.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class SearchRestController {
    @Autowired
    SearchService searchService;
    @Autowired
    WordService wordService;
    @Autowired
    public ConcurrentHashMap<String, Integer> vocabulary;
    @Autowired
    public AtomicInteger WORD_ID;

    @CrossOrigin
    @RequestMapping(value = "/buscar", params = {"palabra"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar(@RequestParam("palabra") String palabra) {
        try {
            Document[] documentos = wordService.buscar(palabra);
            return new ResponseEntity<>(documentos, HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<Exception>(e, HttpStatus.NO_CONTENT);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/indexar", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> indexar() throws IOException, GeneralSecurityException, InterruptedException {
        try {
            vocabulary = wordService.obtenerVocabularioActual();
            if (vocabulary.size() > 0) {
                WORD_ID.getAndSet(wordService.obtenerUltimoId());
            }
            searchService.indexar();
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<Exception>(e, HttpStatus.NO_CONTENT);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/setmax", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> setmax() {
        try {
            wordService.setMax();
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<Exception>(e, HttpStatus.NO_CONTENT);
        }
    }
}
