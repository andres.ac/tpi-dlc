package com.api.docsearch.builders;

import com.api.docsearch.data.PostListRepository;
import com.api.docsearch.data.VocabularyRepository;
import com.api.docsearch.googledrive.DriveDownloader;
import com.api.docsearch.googledrive.DriveService;
import com.api.docsearch.googledrive.FileParser;
import com.api.docsearch.models.Document;
import com.api.docsearch.models.ManageModel;
import com.api.docsearch.models.PostList;
import com.api.docsearch.models.VocabularyEntry;
import com.api.docsearch.service.SearchServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Service("indexService")
public class Indexer {
    @Autowired
    Boolean termino;
    @Autowired
    VocabularyRepository vocabularyRepository;
    @Autowired
    PostListRepository postListRepository;
    @Autowired
    ConcurrentHashMap<String, VocabularyEntry> table;
    @Autowired
    DriveDownloader driveDownloader;
    @Autowired
    Boolean terminoIndexer;
//    @Lazy
//    @Autowired
//    Flusher flusherService;

    @Autowired
    ArrayList<ManageModel> dictionaries;

    @Async
    public void indexar() {
        Document temp;
        while (true) {
            if (driveDownloader.isFinished()) break;
            if ((temp = driveDownloader.getNext()) != null) {
                parseFile(temp);
            } else break;
        }
        terminoIndexer = true;
    }
    public void parseFile(Document file) {
        ManageModel d = new ManageModel(file);
        String[] str = file.getFile().split("[^\\p{Alpha}]");//("[(?U)\\P{L}+\\s]");
        for (int i = 0; i < str.length; i++) {
            str[i] = checkWord(str[i]);
            if (!str[i].equals(" ") && !str[i].isEmpty() && !str[i].equals("")) {
                d.merge(str[i].toLowerCase(), 1, Integer::sum);
            }
        }
        dictionaries.add(d);
    }

    public String checkWord(String word) {
        word = word.replaceAll("([.,\\-\"()'°ª:;¿?_*|~€¬&=!¡<>\\[\\]#@«»$%]|[0-9])+", " ");
        return word.toLowerCase();
    }

    public boolean isFinished() {
        return (terminoIndexer && dictionaries.isEmpty());
    }

    public void setFinishedfFalse() {
        terminoIndexer = false;
    }

    public ManageModel getNext() {
        return dictionaries.size() != 0 ? dictionaries.remove(dictionaries.size() - 1) : null;
    }

    public  ArrayList<ManageModel> getDictionaries() {
        return dictionaries;
    }

}
