package com.api.docsearch.builders;

import com.api.docsearch.data.DocumentRepository;
import com.api.docsearch.data.PostListRepository;
import com.api.docsearch.data.VocabularyRepository;
import com.api.docsearch.models.Document;
import com.api.docsearch.models.ManageModel;
import com.api.docsearch.models.PostList;
import com.api.docsearch.models.VocabularyEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@Service("flusherService")
public class Flusher {
    @Autowired
    Indexer indexer;
    @Autowired
    DocumentRepository documentRepository;
    @Autowired
    VocabularyRepository vocabularyRepository;
    @Autowired
    PostListRepository postListRepository;
    @Autowired
    public ConcurrentHashMap<String, Integer> vocabulary;
    @Autowired
    public AtomicInteger WORD_ID;

    private volatile Integer pr = 0;

    public void flush() {
        ManageModel manageModel;
        while (true) {
            if (indexer.isFinished()) break;
            if ((manageModel = indexer.getNext()) != null) {
                insertar(manageModel);
            } else break;
        }
    }

    public void insertar(ManageModel manageModel) {
        ConcurrentHashMap<String, Integer> tempWords = manageModel.getDictionary();
        Document doc = manageModel.getFile();
        documentRepository.save(doc);
        flushDictionary(tempWords, doc.getIdDocument());
    }

    public void flushDictionary(ConcurrentHashMap<String, Integer> tempWords, int docId) {
        int lasElement = tempWords.entrySet().toArray().length;
        List<VocabularyEntry> lsVoc = new ArrayList<>();
        List<PostList> lsPost = new ArrayList<>();

        int contador = 0;
        for (Map.Entry<String, Integer> e : tempWords.entrySet()) {
            contador++;
            VocabularyEntry v;
            PostList p;
            if (vocabulary.containsKey(e.getKey())) {
                //insert(e.getKey(), vocabulary.get(e.getKey()), docId, e.getValue());
                v = new VocabularyEntry(vocabulary.get(e.getKey()), e.getKey(), 1, 1);
                p = new PostList(docId, vocabulary.get(e.getKey()), e.getValue());
            } else {
                WORD_ID.incrementAndGet();
                //insert(e.getKey(), prueba, docId, e.getValue());
                v = new VocabularyEntry(WORD_ID.get(), e.getKey(), 1, 1);
                p = new PostList(docId, WORD_ID.get(), e.getValue());
                vocabulary.put(e.getKey(), WORD_ID.get());;
            }
            lsVoc.add(v);
            lsPost.add(p);
            if (contador == lasElement) {
                vocabularyRepository.saveAll(lsVoc);
                postListRepository.saveAll(lsPost);
            }
        }

        //WORD_ID.incrementAndGet();
    }

    public void addWordToVocabulary(String word, int prueba) {
        vocabulary.put(word, prueba);
    }

    public void insert(String word, int idWord, int docId, int frequency) {
        vocabularyRepository.save(new VocabularyEntry(idWord, word, 1, 1));
        postListRepository.save(new PostList(docId, idWord, frequency));
    }
}
