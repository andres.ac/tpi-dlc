package com.api.docsearch.models;

import java.io.Serializable;
import java.util.Objects;

public class PostListItem implements Serializable {
    private int idDocument;
    private int idWord;

    public int getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(int idDocument) {
        this.idDocument = idDocument;
    }

    public int getIdWord() {
        return idWord;
    }

    public void setIdWord(int idWord) {
        this.idWord = idWord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostListItem that = (PostListItem) o;
        return idDocument == that.idDocument &&
                idWord == that.idWord;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDocument, idWord);
    }

}
