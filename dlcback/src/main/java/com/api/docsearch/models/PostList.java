/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.docsearch.models;

import javax.jdo.annotations.Transactional;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "postlist")
@IdClass(PostListItem.class)
public class PostList implements Serializable {
    @Id
    @Column(name = "id_document")
    private int idDocument;
    @Id
    @Column(name = "id_word")
    private Integer idWord;
    @Column(name = "frequency")
    private Integer frequency;
    @Transient
    private String word;

    public PostList() {
    }

    public PostList(int idDocument, Integer idWord, Integer frequency) {
        this.idDocument = idDocument;
        this.idWord = idWord;
        this.frequency = frequency;
    }

    public int getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(int idDocument) {
        this.idDocument = idDocument;
    }

    public Integer getIdWord() {
        return idWord;
    }

    public void setIdWord(Integer idWord) {
        this.idWord = idWord;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostList postlist = (PostList) o;
        return idDocument == postlist.idDocument &&
                idWord == postlist.idWord &&
                Objects.equals(frequency, postlist.frequency);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDocument, idWord, frequency);
    }
}
