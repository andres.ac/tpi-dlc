package com.api.docsearch.models;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

@Component
public class ManageModel {
    private ConcurrentHashMap<String, Integer> dictionary;
    private Document file;

    public ManageModel() {

    }

    public ManageModel(Document file) {
        this.dictionary = new ConcurrentHashMap<>();
        this.file = file;
    }

    public Document getFile() {
        return file;
    }

    public ConcurrentHashMap<String, Integer> getDictionary() {
        return dictionary;
    }

    public void merge(String key,
                      Integer value,
                      BiFunction<? super Integer, ? super Integer, ? extends Integer> remappingFunction) {
        dictionary.merge(key, value, remappingFunction);
    }

}
