/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.docsearch.models;

import javax.jdo.annotations.Transactional;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "vocabulary")
public class VocabularyEntry implements Serializable {

    @Id
    @Column(name = "id_word")
    private Integer idWord;
    @Column(name = "word")
    private String word;
    @Column(name = "max_frequency")
    private Integer maxFrequency;
    @Column(name = "max_documents")
    private Integer maxDocuments;
    @Transient
    private boolean grabado;
    @Transient
    private Integer idDocumento;
    @Transient
    private Integer frequency;

    public VocabularyEntry() {
    }


    public VocabularyEntry(Integer idWord, String word, Integer maxFrequency, Integer maxDocuments) {
        this.idWord = idWord;
        this.word = word;
        this.maxFrequency = maxFrequency;
        this.maxDocuments = maxDocuments;
    }

    public Integer getIdWord() {
        return idWord;
    }

    public void setIdWord(Integer idWord) {
        this.idWord = idWord;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getMaxFrequency() {
        return maxFrequency;
    }

    public void setMaxFrequency(Integer maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    public Integer getMaxDocuments() {
        return maxDocuments;
    }

    public void setMaxDocuments(Integer maxDocuments) {
        this.maxDocuments = maxDocuments;
    }

    public boolean isGrabado() {
        return grabado;
    }

    public void setGrabado(boolean grabado) {
        this.grabado = grabado;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VocabularyEntry word1 = (VocabularyEntry) o;
        return idWord == word1.idWord &&
                Objects.equals(word, word1.word) &&
                Objects.equals(maxFrequency, word1.maxFrequency) &&
                Objects.equals(maxDocuments, word1.maxDocuments);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idWord, word, maxFrequency, maxDocuments);
    }
}
