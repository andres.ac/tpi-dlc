package com.api.docsearch.googledrive;

import com.api.docsearch.builders.Flusher;
import com.api.docsearch.builders.Indexer;
import com.api.docsearch.data.DocumentRepository;
import com.api.docsearch.data.VocabularyRepository;
import com.api.docsearch.models.Document;
import com.api.docsearch.service.WordService;
import com.google.api.services.drive.model.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class DriveDownloader {
    @Autowired
    DriveService driveService;
    @Autowired
    ArrayList<Document> documentos;
    @Autowired
    Boolean termino;
    @Lazy
    @Autowired
    Indexer indexerService;
    @Autowired
    DocumentRepository documentRepository;
    @Autowired
    Flusher flusherService;

    public DriveDownloader() {
    }

    @PostConstruct
    public void obtenerDocumentos(){
        documentos = new ArrayList<>(documentRepository.obtenerDocumentosCargados());
    }

    public void download(File file) throws IOException, InterruptedException {
        OutputStream outputStream = new ByteArrayOutputStream();
        driveService.getDrive().files().get(file.getId())
                .executeMediaAndDownloadTo(outputStream);

        Document temp;
        temp = new Document();
        temp.setUrl(file.getWebContentLink());
        temp.setvUrl(file.getWebViewLink());
        temp.setDocName(file.getName());
        temp.setFile(outputStream.toString());

        documentos.add(temp);
        indexerService.indexar();
    }

    public boolean isFinished() {
        return (termino && documentos.isEmpty());
    }

    public void setFinishedFalse() {
        termino = false;
    }

    public Document getNext() {
        return documentos.size() != 0 ? documentos.remove(documentos.size() - 1) : null;
    }

    public ArrayList<Document> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(ArrayList<Document> documentos) {
        this.documentos = documentos;
    }
}
