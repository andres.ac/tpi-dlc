package com.api.docsearch;


import com.api.docsearch.models.Document;
import com.api.docsearch.models.ManageModel;
import com.api.docsearch.models.PostList;
import com.api.docsearch.models.VocabularyEntry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages = "com.api.builders")
public class Config {
    @Bean
    ArrayList<Document> documentos() {
        return new ArrayList<>();
    }

    @Bean
    Boolean termino() {
        return new Boolean(false);
    }

    @Bean
    Boolean terminoIndexer() {
        return new Boolean(false);
    }

    @Bean
    ArrayList<ManageModel> dictionaries() {
        return new ArrayList<>();
    }

    @Bean
    ConcurrentHashMap<String, VocabularyEntry> table() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    ConcurrentHashMap<String, Integer> vocabulary() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    AtomicInteger WORD_ID() {
        return new AtomicInteger(0);
    }

    @Bean
    AtomicInteger NUMBER_OF_DOCUMENTS() {
        return new AtomicInteger(0);
    }

    @Bean
    AtomicInteger HILOS_CONTROL() {
        return new AtomicInteger(0);
    }
}