import {Component, OnChanges, OnInit} from '@angular/core';
import { DocumentService } from './documents.service';
import { Documento } from '../../models/documento';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import {element} from "protractor";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit,OnChanges {
  palabra: string = '';
  documentos: Documento[];

  constructor(private documentService: DocumentService, private spinner: NgxSpinnerService
    , private toastr: ToastrService) { }

  ngOnInit() { }

  public ngOnChanges(): void {
    document.getElementById('datatable').scrollIntoView({behavior: 'smooth'});
  }

  buscar() {
    this.spinner.show();
    this.documentService.buscar(this.palabra).subscribe(
      data => {
        if (data) {
          this.documentos = data;
          this.spinner.hide();
          document.getElementById('datatable').scrollIntoView({behavior: 'smooth'});
        } else {
          this.toastr.error('No se encontraron documentos con esa palabra!', 'Error!', {
            timeOut: 3000
          });
        }
      },
      error => {
        this.toastr.error('No hay conexión con la base de datos.!', 'Error!', {
          timeOut: 3000
        });
        this.spinner.hide();
      }
    );
  }

  descargar(url: string) {
    window.location.href = url;

  }

  indexar() {
    this.documentService.indexar().subscribe((data)=> {});

  }

  setMax() {
    this.documentService.setMax().subscribe((data) => {
        this.spinner.hide();
        this.toastr.info('Se proceso con éxito', 'Info!', {
          timeOut: 3000
        });

      },
      error => {
        this.toastr.error('No hay conexión con la base de datos.!', 'Error!', {
          timeOut: 3000
        });
        this.spinner.hide();
      }
    );
  }
}
