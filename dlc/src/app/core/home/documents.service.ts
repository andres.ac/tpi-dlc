import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../app.config';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Documento} from '../../models/documento';

@Injectable()
export class DocumentService {

  constructor(private http: HttpClient, private config: AppConfig, private router: Router) {
  }

  buscar(palabra: string) {
    return this.http.get<Documento[]>(`${this.config.apiUrl}/buscar?palabra=${palabra}`);
  }

  indexar() {
    return this.http.get<any>(`${this.config.apiUrl}/indexar`);
  }

  setMax() {
    return this.http.get(`${this.config.apiUrl}/setmax`);
  }
}
