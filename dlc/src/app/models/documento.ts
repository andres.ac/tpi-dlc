export class Documento {
    idDocument: number;
    docName: string;
    url: string;
    vUrl: string;
    file: any;
    ranking: number;
}